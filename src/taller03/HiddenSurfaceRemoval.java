package taller03;
/**
 * Created by AndresGutierrez on 05/03/2016.
 */
import java.util.*;
import java.util.List;

import processing.core.PApplet;
import processing.core.PShape;
import remixlab.dandelion.core.Camera;
import remixlab.dandelion.geom.Vec;
import remixlab.proscene.Scene;

//HSR
public class HiddenSurfaceRemoval extends PApplet {

    private Scene scene, auxScene;
    private List<PShape> shapes;
    OctreeNode root;

    @Override
    public void settings() {
        size(640, 720, P3D);
    }

    public void setup() {
        frameRate(30);
        Vec p1 = new Vec(-500, -500, -500);

        scene = new Scene(this, createGraphics(640, 360, P3D));
        scene.enableBoundaryEquations();

        auxScene = new Scene(this, createGraphics(640, 360, P3D), 0, 360);
        auxScene.setRadius(500);
        auxScene.showAll();
        shapes = buildComplexScene();
        root = new OctreeNode(p1,1000,shapes,4);
        root.buildTree(shapes);
        //root.printTree(root);
    }

    public List<PShape> buildComplexScene() {
        List<PShape> shapes = new LinkedList<PShape>();

        Random random = new Random();
        int numx = 50, numy =25;
        for (int i = 0; i < numx; i++) {
            for (int j = 0; j < numy; j++) {
                int x = 20 * (i - numx / 2);
                int y = 20 * (j - numy / 2);
                shapes.addAll(createBox(x, y, 0, 10, random.nextInt(20) + 5));
            }
        }

        return shapes;
    }

    public List<PShape> createBox(int x, int y, int z, int w, int h) {
        // For closed polygons, use normal vectors facing outward
        PShape quad1 = createShape();
        quad1.beginShape(QUAD);
        quad1.vertex(x, y + w, z + h);
        quad1.vertex(x, y, z + h);
        quad1.vertex(x + w, y, z + h);
        quad1.vertex(x + w, y + w, z + h);
        quad1.endShape();

        PShape quad2 = createShape();
        quad2.beginShape(QUAD);
        quad2.vertex(x + w, y, z + h);
        quad2.vertex(x + w, y, z);
        quad2.vertex(x + w, y + w, z);
        quad2.vertex(x + w, y + w, z + h);
        quad2.endShape();

        PShape quad3 = createShape();
        quad3.beginShape(QUAD);
        quad3.vertex(x + w, y + w, z);
        quad3.vertex(x, y + w, z);
        quad3.vertex(x, y + w, z + h);
        quad3.vertex(x + w, y + w, z + h);
        quad3.endShape();

        PShape quad4 = createShape();
        quad4.beginShape(QUAD);
        quad4.vertex(x, y + w, z);
        quad4.vertex(x + w, y + w, z);
        quad4.vertex(x + w, y, z);
        quad4.vertex(x, y, z);
        quad4.endShape();

        PShape quad5 = createShape();
        quad5.beginShape(QUAD);
        quad5.vertex(x, y + w, z + h);
        quad5.vertex(x, y + w, z);
        quad5.vertex(x, y, z);
        quad5.vertex(x, y, z + h);
        quad5.endShape();

        PShape quad6 = createShape();
        quad6.beginShape(QUAD);
        quad6.vertex(x, y, z);
        quad6.vertex(x + w, y, z);
        quad6.vertex(x + w, y, z + h);
        quad6.vertex(x, y, z + h);
        quad6.endShape();

        List<PShape> box = new LinkedList<PShape>();
        box.add(quad1);
        box.add(quad2);
        box.add(quad3);
        box.add(quad4);
        box.add(quad5);
        box.add(quad6);
        return box;
    }

    public void draw() {
        handleMouse();
        surface.setTitle("Frames: " + frameRate);

        scene.pg().beginDraw();
        scene.beginDraw();
        mainDrawing(scene);
        scene.endDraw();
        scene.pg().endDraw();
        image(scene.pg(), 0, 0);

        auxScene.pg().beginDraw();
        auxScene.beginDraw();
        mainDrawing(auxScene);

        auxScene.pg().pushStyle();
        auxScene.pg().stroke(255, 255, 0);
        auxScene.pg().fill(255, 255, 0, 160);
        auxScene.drawEye(scene.eye());
        auxScene.pg().popStyle();

        auxScene.endDraw();
        auxScene.pg().endDraw();
        image(auxScene.pg(), auxScene.originCorner().x(), auxScene.originCorner().y());
    }

    public void mainDrawing(Scene s) {
        s.pg().background(0);
        if(!s.equals(auxScene)) {
            root.drawShape(s.camera());
        }

        for (PShape shape : shapes) {
            s.pg().shape(shape);
        }

    }

    public void handleMouse() {
        if (mouseY < 360) {
            scene.enableMotionAgent();
            scene.enableKeyboardAgent();
            auxScene.disableMotionAgent();
            auxScene.disableKeyboardAgent();
        } else {
            scene.disableMotionAgent();
            scene.disableKeyboardAgent();
            auxScene.enableMotionAgent();
            auxScene.enableKeyboardAgent();
        }
    }

    public static void main(String[] args) {
        PApplet.main(HiddenSurfaceRemoval.class.getCanonicalName());
    }

}

class OctreeNode{
    private Vec p1;
    private float width;
    private List<OctreeNode> children;
    private List<PShape> shapes;
    private int deep;

    public OctreeNode(Vec p1, float width, List<PShape> shapes,int deep) {
        this.p1 = p1;
        this.width = width;
        children = new ArrayList<OctreeNode>();
        this.shapes = new ArrayList<PShape>();
        this.deep = deep;
        for (PShape shape : shapes) {
            boolean isInside = (p1.x() <= shape.getVertex(0).x && p1.x() + width >= shape.getVertex(3).x) &&
                    (p1.y() <= shape.getVertex(0).y && p1.y() + width >= shape.getVertex(3).y) &&
                    (p1.z() <= shape.getVertex(0).z && p1.z() + width >= shape.getVertex(3).z);
            if (isInside) {
                this.shapes.add(shape);
            }
        }

    }
    public void buildTree(List<PShape> shapes){
        if(deep>0){
            for(int x=0; x<2; x++){
                for(int y=0; y<2; y++){
                    for(int z=0; z<2; z++){
                        OctreeNode node = new OctreeNode(new Vec(p1.x()+(width/2*x),p1.y()+(width/2*y),p1.z()+(width/2*z))
                                ,width/2,shapes,deep-1);
                        children.add(node);
                        node.buildTree(shapes);

                    }
                }
            }
        }
    }
    public void drawShape(Camera camera){
        if(camera.boxVisibility(p1,new Vec(p1.x()+width,p1.y()+width,p1.z()+width))==Camera.Visibility.VISIBLE) {
            for (PShape shape : shapes) {
                if(camera.isFaceFrontFacing(Scene.toVec(shape.getVertex(0)),Scene.toVec(shape.getNormal(0)))) {
                    shape.setVisible(true);
                }else{
                    shape.setVisible(false);
                }

            }

        }
        else if(camera.boxVisibility(p1,new Vec(p1.x()+width,p1.y()+width,p1.z()+width))==Camera.Visibility.SEMIVISIBLE){

            if(children.isEmpty()){
                for (PShape shape:shapes){
                    shape.setVisible(true);

                }
            }
            for(OctreeNode node:children){
                node.drawShape(camera);
            }
        }else{
            for (PShape shape : shapes) {
                shape.setVisible(false);

            }
        }
    }
}