package taller04;

import java.util.Comparator;

import processing.core.*;
import remixlab.dandelion.core.Camera;
import remixlab.dandelion.geom.Vec;
import remixlab.proscene.Scene;

public class TestComparator implements Comparator<PShape> {

	private Camera camera;

	public TestComparator(Camera camera) {
		super();
		this.camera = camera;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	@Override
	public int compare(PShape o1, PShape o2) {
		/************************************************
		 * 				Algoritmo del Pintor			*
		 * _____________________________________________*
		 * 												*
		 * Uso de las pruebas del algoritmo del pintor	*
		 ************************************************/

		// 1º Prueba:

		if (traslape(o1, o2) == false) {
			System.out.println(o1.getName() + " 1º NO TRASLAPE " + o2.getName());
			return 0;
		}

		// 2º Prueba:

		else if (detrasDe(o1, o2) == o1.getVertexCount()) {
			System.out.println("2° " + o1.getName() + " esta ATRAS de " + o2.getName());
			return -1;
		}
		// 3º Prueba
		else if (enfreteDe(o1, o2) == true) {
			System.out.println(o1.getName() + " 3º esta al FRENTE de " + o2.getName());
			return -1;
		}
		System.out.println("Fallan pruebas => " + o1.getName() + " esta atras de " + o2.getName());
		return 1;



	}
	/**
	 * 1º Traslape, prueba del algoritmo del pintos
	 *
	 * @param o1
	 * @param o2
	 * @return
	 */
	private boolean traslape(PShape o1, PShape o2) {
		// Translape con respecto a X
		float intersectionMax = Math.min(maxX(o1), maxX(o2));
		float intersectionMin = Math.max(minX(o1), minX(o2));
		if (intersectionMin < intersectionMax) {
			return true;
		}
		// Translape con respecto a Y
		intersectionMax = Math.min(maxY(o1), maxY(o2));
		intersectionMin = Math.max(minY(o1), minY(o2));
		if (intersectionMin < intersectionMax) {
			return true;
		}
		return false;
	}

	/**
	 * 2º Prueba Numero de puntos de tA que estan detras de tB
	 *
	 * @param o1
	 * @param o2
	 * @return
	 */
	private int detrasDe(PShape o1, PShape o2) {
		boolean negarNormal = false;

		//TODO: Cual index se debe pasar para  getNotmal(index)

		//Si la Normal respecto a Z es menor a cero
		if (o2.getNormalZ(0) < 0) {
			negarNormal = true;
		}
		int n = 0;
		Float[] angles = this.anglesFromNormal(o2, o1, negarNormal);
//		for (float angle : angles) {
//			//Convierto en "angle" en grados y si este es menor igual a 90 grados
//			if ((float) Math.toDegrees(angle) <= 90) {
//				n++;
//			}
//		}
		return n;
	}

	private boolean enfreteDe(PShape o1, PShape o2) {
		if (this.detrasDe(o1, o2) == 0) {
			return false;
		}
		boolean negarNormal = false;
		if (o2.getNormalZ(0) < 0) {
			negarNormal = true;
		}
		for (float angle : this.anglesFromNormal(o2, o1, negarNormal)) {
			if ((float) Math.toDegrees(angle) > 90) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Este es para hallar  los Ángulos entre la normal de o1 y los puntos de o2
	 *
	 * @param o1
	 * @param o2
	 * @param negarNormal
	 * @return
	 */
	private Float[] anglesFromNormal(PShape o1, PShape o2, boolean negarNormal) {
		Float[] anglesArray = new Float[o2.getVertexCount()];

		return anglesArray;
	}
	public int zCompare(PShape o1, PShape o2) {
		// Ordenar del mayor al menor valor de z
		int result = (int) Math.signum(maxZ(o2) - maxZ(o1));
		if (result == 0) {
			result = -1;
		}
		return result;
	}

	private float maxX(PShape tmp) {
		return Math.max(tmp.getVertexX(0), Math.max(tmp.getVertexX(1), tmp.getVertexX(2)));
	}

	public float minX(PShape tmp) {
		return Math.min(tmp.getVertexX(0), Math.min(tmp.getVertexX(1), tmp.getVertexX(2)));
	}

	public float maxY(PShape tmp) {
		return Math.max(tmp.getVertexY(0), Math.max(tmp.getVertexY(1), tmp.getVertexY(2)));
	}

	public float minY(PShape tmp) {
		return Math.min(tmp.getVertexY(0), Math.min(tmp.getVertexY(1), tmp.getVertexY(2)));
	}

	public float maxZ(PShape tmp) {
		return Math.max(tmp.getVertexZ(0), Math.max(tmp.getVertexZ(1), tmp.getVertexZ(2)));
	}

	public float minZ(PShape tmp) {
		return Math.min(tmp.getVertexZ(0), Math.min(tmp.getVertexZ(1), tmp.getVertexZ(2)));
	}

}